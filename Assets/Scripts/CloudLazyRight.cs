﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudLazyRight : MonoBehaviour {

	public float TraverseExtent = 4f;
	public float Speed = 0.5f;
	public Vector3 movement;

	private void Start()
	{
		movement = new Vector3(Speed, 0, 0);
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.x >= TraverseExtent || transform.position.x <= -TraverseExtent)
		{
			movement.x = -movement.x;
			if (transform.position.x >= TraverseExtent)
            {
				transform.position = new Vector3(TraverseExtent, transform.position.y, transform.position.z);
			}
            else
            {
				transform.position = new Vector3(-TraverseExtent, transform.position.y, transform.position.z);
			}
		}
		transform.Translate(movement * Time.deltaTime);
	}
}
