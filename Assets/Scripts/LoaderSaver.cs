﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LoaderSaver : MonoBehaviour {

	public void AttemptSave(){
		// Run any guard checks then the Save function
		if (!SaveSDC()){
			Debug.Log("Save function not written yet");
		}
		else{
			Debug.Log("Save function successful");
		}
	}

	public void AttemptLoad(){
		// Run any guard checks, then the Load function
		SaveDataClass SDC = LoadSDC();
	}

	private bool SaveSDC()
	{
		bool isTrue = true;
		Debug.Log("Trying to save");
		if (isTrue) return false;

		// Formatter and streamers required for saving.
		BinaryFormatter binform = new BinaryFormatter();
		FileStream stream = new FileStream(Application.persistentDataPath + "/Profiles.bin", FileMode.Create);

		if (File.Exists(Application.persistentDataPath + "/Profiles.bin")){

			SaveDataClass SDC = new SaveDataClass();
			// Populate this new file with required data
			SDC.SavedValue = 69;

			binform.Serialize(stream, SDC);	// Save the required data
		}
		stream.Close();
		
		return true;
	}

	private SaveDataClass LoadSDC(){
		if (File.Exists(Application.persistentDataPath + "/Profiles.bin"))
		{
			BinaryFormatter binform = new BinaryFormatter();
			FileStream stream = new FileStream(Application.persistentDataPath + "/Profiles.bin", FileMode.Open);

			SaveDataClass SDC = binform.Deserialize(stream) as SaveDataClass;
			stream.Close();

			return SDC;
		}
		else
		{
			Debug.Log("Save File Doesn't exist - default data returned");
			return new SaveDataClass();
		}

	}
}
